from django.urls import path
from . import views

urlpatterns = [
    path('', views.scraper_data, name="Index"),
    path('search/', views.search, name="search") 
]