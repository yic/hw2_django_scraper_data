from abc import ABC, abstractmethod
from bs4 import BeautifulSoup
import requests
from datetime import datetime

# 票券網站抽象類別
class Website(ABC):

    def __init__(self, city_name):
        self.city_name = city_name  # 城市名稱屬性

    @abstractmethod
    def scrape(self):  # 爬取票券抽象方法
        pass


# KKday網站
class Kkday(Website):

    def scrape(self):

        result = []  # 回傳結果

        if self.city_name:  # 如果城市名稱非空值

            # 取得傳入城市的所有一日遊票券 #A01-001-00018 #A01-001-00003 A01-001-00019
            header = {"user-agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1"}
            response = requests.get(
                f"https://m.kkday.com/zh-tw/product/ajax_get_productlist?countryCd=A01-001&sort=popularity&page=1&city={self.city_name}&cat=TAG_2_5&glang=&fprice=*&eprice=*&availStartDate=&availEndDate=",headers=header)
                  
            # 資料
            activities = response.json()["data"]['product_list']

            for activity in activities:

                # 票券名稱
                title = activity["name"]

                # 票券詳細內容連結
                link = activity["url"]

                # 票券價格
                price = f'NT$ {int(activity["price"]):,}'

                # 最早可使用日期
                booking_date = datetime.strftime(datetime.strptime(
                    activity["earliest_sale_date"], "%Y%m%d"), "%Y-%m-%d")

                # 評價
                star = str(activity["rating_star"])[0:3] if activity["rating_star"] else "無"

                result.append(
                    dict(title=title, link=link, price=price, booking_date=booking_date, star=star, source="https://cdn.kkday.com/m-web/assets/img/favicon.png"))

        return result


# KLOOK客路網站





