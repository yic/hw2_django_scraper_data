from http.client import HTTPResponse
import imp
from django.shortcuts import render
from .scrapers import  Kkday #Klook,
from .models import restaurant_data
from .select_form import select_data_form
from datetime import datetime,date
from apscheduler.schedulers.blocking import BlockingScheduler

def my_job(text=""):
    print(text, 'my_job1 is running, Now is %s' % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


# Create your views here.
def scraper_data(request): #抓資料進去資料庫中

    # sched = BlockingScheduler()
    # job = sched.add_job(my_job, 'interval', hours=2)
    # sched.start()

    kkday = Kkday(request.POST.get("city_name"))
    result  = kkday.scrape()
    # print(result)
    for i in range(0,len(result)):
        tmp = []
        for k,v in result[i].items():
            if k == 'title' or k == 'price' or k == 'star':
                tmp.append(v)
        # print(tmp)
        unit1 = restaurant_data.objects.create(city = request.POST.get("city_name"),
                                                title=tmp[0],price=tmp[1],star=tmp[2])
         
        unit1.save()                                     
    
    context = {"tickets":  kkday.scrape()} #klook.scrape() +
    
    return  render(request, "index.html", context)

def search(request):

    # form = select_data_form()
    
    rdata = restaurant_data.objects.filter(city=request.POST.get("city_name")).order_by('id')

    return  render(request, "search.html",locals()) #locals()


