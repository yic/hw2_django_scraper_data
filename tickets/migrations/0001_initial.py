# Generated by Django 4.0.4 on 2022-08-31 14:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='restaurant_data',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('city', models.CharField(max_length=50, null=True)),
                ('title', models.CharField(max_length=50)),
                ('price', models.CharField(max_length=50)),
                ('star', models.CharField(max_length=50)),
            ],
        ),
    ]
