from django import forms
from .models import restaurant_data

class select_data_form(forms.Form):
    select_data = forms.ModelChoiceField(queryset=restaurant_data.objects.values_list('city'),
                                        widget=forms.Select(attrs={'class': "selectpicker"}))
                           
    